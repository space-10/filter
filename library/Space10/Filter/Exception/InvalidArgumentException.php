<?php
namespace Space10\Filter\Exception;

/**
 * Exception thrown if an argument is not of the expected type.
 */
class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface
{
}
