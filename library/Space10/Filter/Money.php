<?php
namespace Space10\Filter;

use Zend\Filter\FilterInterface;

/**
 * Money format filter
 */
class Money implements FilterInterface
{

    /**
     *
     * @var string
     */
    protected $format;

    /**
     *
     * @param string $format
     */
    public function __construct($format)
    {
        $this->format = $format;
    }

    /**
     *
     * @param float $value
     * @return string
     */
    public function filter($value)
    {
        return money_format($this->format, $value);
    }
}
