<?php

namespace Space10\Filter;

use Zend\Filter\Exception;

class TransliterateUrl extends Transliterate
{

    /**
     * Returns the result of filtering $value
     *
     * @param  mixed $value
     *
     * @throws Exception\RuntimeException If filtering $value is impossible
     * @return mixed
     */
    public function filter($value)
    {
        $value = preg_replace("#[^.a-z0-9/_| -]#i", '', parent::filter($value));
        $value = preg_replace("#[/_| -]+#", '-', $value);
        return trim(strtolower($value), '-');
    }
}
