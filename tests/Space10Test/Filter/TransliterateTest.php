<?php
namespace Space10Test\Filter;

use Space10\Filter\Transliterate;

/**
 */
class TransliterateTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var Transliterate
     */
    protected $filter;

    protected function setUp()
    {
        $this->filter = new Transliterate();
    }

    public function filterDataProvider()
    {
        return [
            ['™ @', 'tm at'],
            ['äÄöÖüÜµ', 'aAoOuUµ'],
            ['äåö-ÅÄÖÜé', 'aao-AAOUe'],
            ['äfoåöo Å BÄaÖrÜ é', 'afoaoo A BAaOrU e'],
        ];
    }

    /**
     * @covers ::filter
     * @dataProvider filterDataProvider
     */
    public function testFilter($value, $expected)
    {
        $actual = $this->filter->filter($value);
        $this->assertSame($expected, $actual);
    }
}
