<?php
namespace Space10Test\Filter;

use Space10\Filter\TransliterateUrl;

/**
 */
class TransliterateUrlTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var TransliterateUrl
     */
    protected $filter;

    protected function setUp()
    {
        $this->filter = new TransliterateUrl();
    }

    public function filterDataProvider()
    {
        return [
            ['!"§$%&/()=?`´{[]}\\€@', 'at'],
            ['äÄöÖüÜµ', 'aaoouu'],
            ['äåö-ÅÄÖÜé', 'aao-aaoue'],
            ['äfoåöo Å BÄaÖrÜ é', 'afoaoo-a-baaoru-e'],
            ['!Å§$-%b&a?z)', 'a-baz'],
            ['f!oO"- §b$a%&z', 'foo-baz'],
        ];
    }

    /**
     * @covers ::filter
     * @dataProvider filterDataProvider
     */
    public function testFilter($value, $expected)
    {
        $actual = $this->filter->filter($value);
        $this->assertSame($expected, $actual);
    }
}
